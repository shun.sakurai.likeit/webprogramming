package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.User;

public class UserDao {

    public User findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        try {
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, password);
            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return null;
            }

            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

    public List<User> findAll() {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user WHERE id NOT IN (1);";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }

    public void insert(String loginId, String password,String name, String birthDate){
            Connection conn = null;
            try {
                conn = DBManager.getConnection();

                String insertSQL = "INSERT INTO user (login_id,password,name,birth_date,create_date,update_date) VALUES (?,?,?,?,now(),now())";

                PreparedStatement pStmt= conn.prepareStatement(insertSQL);
                pStmt.setString(1, loginId);
                pStmt.setString(2, password);
                pStmt.setString(3, name);
                pStmt.setString(4, birthDate);

                pStmt.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
    			if (conn != null) {
    				try {
    					conn.close();
    				} catch (SQLException e) {
    					e.printStackTrace();
    				}
    			}
            }
    }

    public User findByUserInfo(int id) {
    	Connection conn = null;
    	try {
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user WHERE id = ?";

            PreparedStatement pStmt = conn.prepareStatement(sql);

            pStmt.setInt(1, id);

            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return null;
            }

			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate  = rs.getDate("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			return new User(loginId, name, birthDate, createDate, updateDate);


        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
       }
    }

    public User findByUserInfo2(int id) {
    	Connection conn = null;
    	try {
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user WHERE id = ?";

            PreparedStatement pStmt = conn.prepareStatement(sql);

            pStmt.setInt(1, id);

            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return null;
            }
            int idData = rs.getInt("id");
			String loginIdData = rs.getString("login_id");

			return new User(idData,loginIdData );


        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
       }
    }

    public void update(String password,String name, String birthDate, String id){
        Connection conn = null;
        try {
            conn = DBManager.getConnection();

            String updateSQL = "UPDATE user SET password = ?,name = ?, birth_date = ?, update_date = now() WHERE id = ?";

            PreparedStatement pStmt= conn.prepareStatement(updateSQL);

            pStmt.setString(1, password);
            pStmt.setString(2, name);
            pStmt.setString(3, birthDate);
            pStmt.setString(4, id);

            pStmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
        }
    }

    public void update2(String name, String birthDate, String id){
        Connection conn = null;
        try {
            conn = DBManager.getConnection();

            String updateSQL = "UPDATE user SET name = ?, birth_date = ?, update_date = now() WHERE id = ?";

            PreparedStatement pStmt= conn.prepareStatement(updateSQL);

            pStmt.setString(1, name);
            pStmt.setString(2, birthDate);
            pStmt.setString(3, id);

            pStmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
        }
    }

    public void delete(String id){
        Connection conn = null;
        try {
            conn = DBManager.getConnection();

            String deleteSQL = "Delete from user WHERE id = ?";

            PreparedStatement pStmt= conn.prepareStatement(deleteSQL);
            pStmt.setString(1, id);

            pStmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
        }
    }

    public User findByLoginId(String loginId) {
        Connection conn = null;
        try {
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user WHERE login_id = ?";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return null;
            }

            String loginIdData = rs.getString("login_id");
            return new User(loginIdData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

    public User findByid() {
        Connection conn = null;
        try {
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user WHERE login_id = admin";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            String loginIdData = rs.getString("login_id");
            return new User(loginIdData );

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

    public User serch(String loginId, String name) {
        Connection conn = null;
        try {
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user WHERE login_Id LIKE '?' OR name LIKE '%?%'";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, name);

            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return null;
            }
            int idData = rs.getInt("id");
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(idData,loginIdData,nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

    public List<User> serchAll(String loginId ,String name , String startbirthdate , String endbirthdate ) {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user WHERE id NOT IN (1)";

            if(!(loginId.equals(""))) {
            	sql += "AND login_id LIKE '"+ loginId + "'";
            }

            if(!(name.equals(""))) {
            	sql += "AND name LIKE '"+ "%" + name + "%" + "'";
            }

            if(!(startbirthdate.equals(""))) {
            	sql += "AND birth_date >= '"+ startbirthdate + "'";
            }

            if(!(endbirthdate.equals(""))) {
            	sql += "AND birth_date <= '"+ endbirthdate + "'";
            }

            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
            	 int id = rs.getInt("id");
                 String loginIdData = rs.getString("login_id");
                 String nameData = rs.getString("name");
                 Date birthDate = rs.getDate("birth_date");
                 String password = rs.getString("password");
                 String createDate = rs.getString("create_date");
                 String updateDate = rs.getString("update_date");
                 User user = new User(id, loginIdData, nameData, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }
}
