<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/userUpdate.css" type="text/css" rel="stylesheet">
</head>
<body>
    <div class = "contain mx-auto">
        <div class = "header">
            <div class = "headerlist">
                <a id="userName">${userInfo.name}さん</a>
                <a id="logout" href="LogoutServlet">ログアウト</a>
            </div>
        </div>

        <div class = "main">
            <h1>ユーザー情報更新</h1>
            	<c:if test = "${errMsg != null}">
        			<div class = "alert">
        				${errMsg}
        			</div>
       			</c:if>
            <form action="UserUpdateServlet" method="post">
            <input type="hidden" name="id" value="${user.id}">
               		 <table class="table">
	                    <tr>
	                        <td>ログインID</td>
	                        <td >${user.loginId}</td>
	                    </tr>
	                    <tr>
	                        <td>パスワード</td>
	                        <td><input class ="text" type="password" name="password1"></td>
	                    </tr>
	                    <tr>
	                        <td>パスワード（確認）</td>
	                        <td ><input class ="text" type="password" name="password2"></td>
	                    </tr>
	                    <tr>
	                        <td>ユーザー名</td>
	                        <td><input class ="text" type="text" name="name" value="${user.name}"></td>
	                    </tr>
	                    <tr>
	                        <td>生年月日</td>
	                        <td><input class ="text" type="text" name="birthDate" value="${user.birthDate}"></td>
	                    </tr>
               		 </table>
               		 <input class ="contact-update" type="submit" value="更新">
            </form>
            <a href=UserListServlet>戻る</a>
        </div>
    </div>
</body>
</html>