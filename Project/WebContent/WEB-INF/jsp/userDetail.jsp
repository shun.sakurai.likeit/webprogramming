<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/userDetail.css" type="text/css" rel="stylesheet">
</head>
<body>
    <div class = "contain mx-auto">
        <div class = "header">
            <div class = "headerlist">
                    <a id="userName">${userInfo.name}さん</a>
                    <a id="logout" href="LogoutServlet">ログアウト</a>
            </div>
        </div>
        	<div class = "main">
           		<h1>ユーザー情報詳細参照</h1>
            	<table class = table>
	                <tr>
	                    <td width="150">ログインID</td>
	                    <td>${user.loginId}</td>
	                </tr>
	                <tr>
	                    <td width="150">ユーザー名</td>
	                    <td>${user.name}</td>
	                </tr>
	                <tr>
	                    <td width="150">生年月日</td>
	                    <td>${user.birthDate}</td>
	                </tr>
	                <tr>
	                    <td width="150">登録日時</td>
	                    <td>${user.createDate}</td>
	                </tr>
	                <tr>
	                    <td width="150">更新日時</td>
	                    <td>${user.updateDate}</td>
	                </tr>
            	</table>
            	<a href="UserListServlet">戻る</a>
       		 </div>
    </div>
</body>
</html>