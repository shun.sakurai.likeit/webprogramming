<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/userDelete.css" type="text/css" rel="stylesheet">
</head>
<body>
    <div class = "contain mx-auto">
        <div class = "header">
            <div class = "headerlist">
         		 <a id="userName">${userInfo.name}さん</a>
                 <a id="logout" href="LogoutServlet">ログアウト</a>
            </div>
        </div>

        <div class = "main">
            <h1>ユーザー削除確認</h1>
            <p>ログインID：${user.loginId}<br>を本当に削除してよろしいでしょうか。</p>

	            <form action="UserListServlet" method="get">
	                 <input class ="contact-box" type="submit" value="キャンセル">
	            </form>

	            <form action="UserDeleteServlet" method="post">
	            <input type="hidden" name="id" value="${user.id}">
	                 <input class ="contact-box" type="submit" value="OK">
	            </form>
        </div>
    </div>
</body>
</html>