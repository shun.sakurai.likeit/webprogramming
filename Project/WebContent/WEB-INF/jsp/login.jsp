<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>

    <meta charset="utf-8">
    <title>ログイン画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/login.css" type="text/css" rel="stylesheet">

</head>
<body>
    <div class = "contain mx-auto">
        <h1>ログイン画面</h1>
        <c:if test = "${errMsg != null}">
        	<div class = "alert">
        		${errMsg}
        	</div>
        </c:if>
        <form action="LoginServlet" method="post">
            <div class="texttype">
                <ul>
                <li>ログインID<input type = "text" name="loginId" ></li>
                <li>パスワード<input type = "password" name="password"></li>
                </ul>
            </div>
            <input class ="contact-login" type="submit" value="ログイン">
        </form>
    </div>
</body>
</html>
