<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ユーザー新規登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/userRegister.css" type="text/css" rel="stylesheet">
</head>
<body>
    <div class = "contain mx-auto">
        <div class = "header">
            <div class = "headerlist">
                    <a href="LogoutServlet" id = "logout">ログアウト</a>
                    <a id = "userName">${userInfo.name}さん</a>
            </div>
        </div>

        <div class = "main">
            <h1>ユーザー新規登録</h1>
             	<c:if test = "${errMsg != null}">
        			<div class = "alert">
        				${errMsg}
        			</div>
       			</c:if>

            <form action="UserRegisterServlet" method="post">
                <table class="table">
                    <tr>
                        <td>ログインID</td>
                        <td ><input class ="text" type="text" name="loginId"></td>
                    </tr>
                    <tr>
                        <td>パスワード</td>
                        <td><input class ="text" type="password" name="password1"></td>
                    </tr>
                    <tr>
                        <td>パスワード（確認）</td>
                        <td ><input class ="text" type="password" name="password2"></td>
                    </tr>
                    <tr>
                        <td>ユーザー名</td>
                        <td><input class ="text" type="text" name="name"></td>
                    </tr>
                    <tr>
                        <td>生年月日</td>
                        <td><input class ="text" type="text" name="birthDate"></td>
                    </tr>
                </table>
                <input class ="contact-search" type="submit" value="登録">
            </form>
            <a href="UserListServlet">戻る</a>
        </div>
    </div>
</body>
</html>