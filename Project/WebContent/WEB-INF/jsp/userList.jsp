<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ユーザー一覧画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/userList.css" type="text/css" rel="stylesheet">
</head>
<body>
    <div class = "contain mx-auto">
        <div class = "header">
            <div class = "headerlist">
                    <a id="userName">${userInfo.name}さん</a>
                    <a id="logout" href="LogoutServlet">ログアウト</a>
            </div>
        </div>

        <div class = "main">
            <h1>ユーザー覧</h1>
            <a id = "userRegister"href="UserRegisterServlet">新規登録</a>
            <form action="UserListServlet" method="post">
                <ul>
                <li>ログインID<input class ="user" type = "text" name="loginId"></li>
                <li>ユーザー名<input class = "user" type = "text" name="name"></li>
                <li>生年月日<input class = "seinengappi1" type="date" name="calendar1" max="9999-12-31"><span>~</span><input class= "seinengappi2" type="date" name="calendar2" max="9999-12-31"></li>
                </ul>
                <input class ="contact-search" type="submit" value="検索">
            </form>
            <table border="1" width=100%>
                <tr>
                    <th >ログインID</th>
                    <th >ユーザー名</th>
                    <th >生年月日</th>
                    <th ></th>
                </tr>

			   <c:forEach var="user" items="${userList}">
	                <tr>
	                    <td>${user.loginId}</td>
	                    <td>${user.name}</td>
	                    <td>${user.birthDate}</td>

	                    <td>
	                    	 <c:if test = "${userInfo.loginId == 'admin'}">
	                    	 	<a href="UserDetailServlet?id=${user.id}" class="blue" >詳細</a>
	                    	 	<a href="UserUpdateServlet?id=${user.id}" class ="green">更新</a>
	                    	 	<a href="UserDeleteServlet?id=${user.id}" class="red">削除</a>
	                    	 </c:if>
	                    	 <c:if test = "${userInfo.loginId != 'admin'}">
	                    	 	<a href="UserDetailServlet?id=${user.id}" class="blue" >詳細</a>
	                    	 	<c:if test = "${userInfo.loginId.equals(user.loginId)}">
	                    	 		<a href="UserUpdateServlet?id=${user.id}" class ="green">更新</a>
	                    	 	</c:if>
	                    	 </c:if>
	                    </td>
	                </tr>
	            </c:forEach>
            </table>
        </div>
    </div>
</body>
</html>
