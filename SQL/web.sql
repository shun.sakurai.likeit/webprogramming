﻿DROP DATABASE usermanagement
CREATE DATABASE usermanagement CHARACTER SET utf8 ;
USE usermanagement;
DROP TABLE user;
CREATE TABLE user(id SERIAL  UNIQUE NOT NULL,
                  login_id varchar(255) UNIQUE NOT NULL,
                  name varchar(255) NOT NULL,
                  birth_date DATE NOT NULL,
                  password varchar(255) NOT NULL,
                  create_date DATETIME NOT NULL,
                  update_date DATETIME NOT NULL);
                  
INSERT into user(id,login_id,name,birth_date,password,create_date,update_date) values (1,'admin','管理者','1998-02-28','password',now(),now());


